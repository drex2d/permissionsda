package ec.edu.espoch.PermissionsDA.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReasonParameterPresenter {
    private Integer id;
    private Float minimumTime;
    private Float maximumTime;
    private Float anticipationTime;
    private String article;
    private Boolean chargeToVacation;
    private ReasonPresenter reasonPresenter;
    private LaborRelationshipPresenter laborRelationshipPresenter;
    private LaborRegimePresenter laborRegimePresenter;
}
