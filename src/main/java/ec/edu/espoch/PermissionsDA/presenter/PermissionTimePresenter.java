package ec.edu.espoch.PermissionsDA.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PermissionTimePresenter {
    private Integer id;
    private Date startDate;
    private Date startTime;
    private Date returnDate;
    private Date returnTime;
    private Integer permissionTime;
}
