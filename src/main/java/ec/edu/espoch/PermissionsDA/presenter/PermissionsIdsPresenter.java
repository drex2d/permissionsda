package ec.edu.espoch.PermissionsDA.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PermissionsIdsPresenter {
    private List<Integer> listPermissionsId;
    private String workerDni;
    private Integer stateId;
}
