package ec.edu.espoch.PermissionsDA.presenter;

import ec.edu.espoch.PermissionsDA.entity.PermissionType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ReasonPresenter {
    private Integer id;
    private String description;
    private String acronym;
    private PermissionTypePresenter permissionTypePresenter;
}
