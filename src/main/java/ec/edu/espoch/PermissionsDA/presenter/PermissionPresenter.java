package ec.edu.espoch.PermissionsDA.presenter;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PermissionPresenter {
    private Integer id;
//    @DateTimeFormat(iso= DateTimeFormat.ISO.DATE)

//    @JsonFormat sonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "America/Guayaquil")
    private Date permissionIssuanceDate;

    private String workerDni;
    private String workerNames;
    private String workerLastNames;

    private ReasonPresenter reasonPresenter;

    private ReasonParameterPresenter reasonParameterPresenter;

    private PermissionStatePresenter permissionStatePresenter;
    @Builder.Default
    private List<PermissionTimePresenter> permissionTimePresenters=new ArrayList<>();
    @Builder.Default
    private List<DocumentPresenter> documentPresenters=new ArrayList<>();
    @Builder.Default
    private List<PermissionLogPresenter> permissionLogPresenters=new ArrayList<>();

}
