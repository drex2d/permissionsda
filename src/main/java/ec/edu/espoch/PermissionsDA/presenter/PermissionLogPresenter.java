package ec.edu.espoch.PermissionsDA.presenter;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PermissionLogPresenter {
    private Integer id;
    private Date logDate;
    private String description;
}
