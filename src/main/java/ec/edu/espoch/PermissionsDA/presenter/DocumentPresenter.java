package ec.edu.espoch.PermissionsDA.presenter;

import ec.edu.espoch.PermissionsDA.entity.Permission;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DocumentPresenter {
    private Integer id;
    private String name;
    private String url;
}
