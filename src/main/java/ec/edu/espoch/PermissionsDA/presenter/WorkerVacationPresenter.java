package ec.edu.espoch.PermissionsDA.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkerVacationPresenter {
    private Integer id;
    private Integer accumulationYear;
    private Double accumulatedDays;
    private Date lastAccumulationDate;
    private String state;
}
