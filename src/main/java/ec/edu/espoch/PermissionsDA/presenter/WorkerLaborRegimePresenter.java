package ec.edu.espoch.PermissionsDA.presenter;

import ec.edu.espoch.PermissionsDA.entity.LaborRegime;
import ec.edu.espoch.PermissionsDA.entity.Worker;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkerLaborRegimePresenter {
    private Integer idWorkerLaborRegime;
    private LaborRegimePresenter laborRegimePresenter;
    private String jobTitle;
    private String workDependency;
}
