package ec.edu.espoch.PermissionsDA.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkerPresenter {
    private Integer id;
    private String dni;
    private String names;
    private String lastNames;
    private String state;
    @Builder.Default
    private List<RolPresenter> rolPresenters = new ArrayList<>();
    private WorkerLaborRelationshipPresenter workerLaborRelationshipPresenter;
    private WorkerLaborRegimePresenter workerLaborRegimePresenter;

    @Builder.Default
    private List<WorkerVacationPresenter> workerVacationPresenters = new ArrayList<>();
//    @Builder.Default
//    private List<PermissionPresenter> workerPermissionPresenters = new ArrayList<>();
//    @Builder.Default
//    private List<PermissionLogPresenter> permissionLogPresenters = new ArrayList<>();
}
