package ec.edu.espoch.PermissionsDA.presenter;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.Timer;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class WorkerLaborRelationshipPresenter {
    private Integer idWorkerLaborRelationship;
    private LaborRelationshipPresenter laborRelationshipPresenter;
    private Date contractStartDate;
    private Date contractEndDate;
    private Date startTime;
    private Date endTime;
}
