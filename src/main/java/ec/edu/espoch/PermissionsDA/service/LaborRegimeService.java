package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.presenter.LaborRegimePresenter;

import java.util.List;

public interface LaborRegimeService {
    List<LaborRegimePresenter> getAll();
}
