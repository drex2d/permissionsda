package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.entity.WorkerVacation;
import ec.edu.espoch.PermissionsDA.repository.WorkerVacationRepository;
import ec.edu.espoch.PermissionsDA.service.WorkerVacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service
public class WorkerVacationServiceImpl implements WorkerVacationService {
    @Autowired
    private WorkerVacationRepository workerVacationRepository;

    @Override
    @Transactional
    @Deprecated
    public void updateAccumulation() {
        Date hoy = new Date();
        int actualYear = hoy.getYear() + 1900;
        workerVacationRepository.findAll().forEach(workerVacation -> {
            if (workerVacation.getState().equals("In Progress")) {
                if (actualYear == workerVacation.getAccumulationYear()) {
                    workerVacation.setAccumulatedDays(workerVacation.getAccumulatedDays() + 0.0822);
                    workerVacation.setLastAccumulationDate(hoy);
                    workerVacationRepository.save(workerVacation);
                    System.out.println("HELLO UPDATE VACATION");
                } else if (actualYear > workerVacation.getAccumulationYear()) {
                    workerVacation.setState("Finished");
                    workerVacationRepository.save(workerVacation);
                    WorkerVacation newWorkerVacation = WorkerVacation.builder()
                            .worker(workerVacation.getWorker())
                            .accumulatedDays(0.0)
                            .lastAccumulationDate(hoy)
                            .accumulationYear(hoy.getYear() + 1900)
                            .state("In Progress")
                            .build();
                    workerVacationRepository.save(newWorkerVacation);
                    System.out.println("HELLO CREATE NEW VACATION");
                }
            }
        });
    }
}
