package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.presenter.WorkerPresenter;

import java.util.List;

public interface WorkerService {
    WorkerPresenter save(WorkerPresenter workerPresenter);

    List<WorkerPresenter> getAll();

    WorkerPresenter fetchWorkerByDni(String dni);

    WorkerPresenter fetchWorkerById(Integer id);

    void delete(String dni);

    List<WorkerPresenter> searchAllByFilter(String filter);
}