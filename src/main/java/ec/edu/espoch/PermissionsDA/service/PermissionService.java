package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.presenter.PermissionPresenter;
import ec.edu.espoch.PermissionsDA.presenter.PermissionsIdsPresenter;

import java.util.List;

public interface PermissionService {
    List<PermissionPresenter> getAll();

    List<PermissionPresenter> getAllByWorkerDni(String dni);

    List<PermissionPresenter> getAllByReasonId(Integer reasonId);

    List<PermissionPresenter> getAllByStateId(Integer stateId);

    List<PermissionPresenter> getAllByStateAndDependency(Integer stateId, String workDependency);

    String save(PermissionPresenter permissionPresenter);

    List<PermissionPresenter> getAlByWorkerDniAndPermissionTypeId(String workerDni, Integer permissionTypeId);

    PermissionPresenter getByPermissionId(Integer permissionId);

    String deleteById(Integer permissionId);

    List<PermissionPresenter> getAllByWorkerDniAndStateId(String workerDni, Integer stateId);

    String changeStatePermission(PermissionsIdsPresenter permissionsIdsPresenter);

    void updatePermissions();

    List<PermissionPresenter> getAllNotificationsByWorkerDni(String workerDni);
}
