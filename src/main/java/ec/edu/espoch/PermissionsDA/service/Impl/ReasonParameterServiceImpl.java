package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.entity.*;
import ec.edu.espoch.PermissionsDA.presenter.*;
import ec.edu.espoch.PermissionsDA.repository.ReasonParameterRepository;
import ec.edu.espoch.PermissionsDA.repository.WorkerRepository;
import ec.edu.espoch.PermissionsDA.service.ReasonParameterService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReasonParameterServiceImpl implements ReasonParameterService {
    @Autowired
    private ReasonParameterRepository reasonParameterRepository;
    @Autowired
    private WorkerRepository workerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ReasonParameterPresenter> getAllByReasonId(Integer idReason) {
        List<ReasonParameterPresenter> reasonParameterPresenters = new ArrayList<>();
        reasonParameterRepository.findAllByReasonId(idReason).forEach(reasonParameter -> {
            ReasonParameterPresenter reasonParameterPresenter = modelMapper.map(reasonParameter, ReasonParameterPresenter.class);
            reasonParameterPresenter.setReasonPresenter(modelMapper.map(reasonParameter.getReason(), ReasonPresenter.class));
            reasonParameterPresenter.getReasonPresenter().setPermissionTypePresenter(modelMapper.map(reasonParameter.getReason().getPermissionType(), PermissionTypePresenter.class));
            reasonParameterPresenter.setLaborRelationshipPresenter(modelMapper.map(reasonParameter.getLaborRelationship(), LaborRelationshipPresenter.class));
            reasonParameterPresenter.setLaborRegimePresenter(modelMapper.map(reasonParameter.getLaborRegime(), LaborRegimePresenter.class));

            reasonParameterPresenters.add(reasonParameterPresenter);
        });
        return reasonParameterPresenters;
    }

    @Override
    public ReasonParameterPresenter getByReasonIdAndWorkerDni(Integer reasonID, String workerDni) {
        Optional<Worker> worker = workerRepository.findByDni(workerDni);
        ReasonParameterPresenter reasonParameterPresenter2 = new ReasonParameterPresenter();
        if (worker.isPresent()) {
            List<ReasonParameterPresenter> reasonParameterPresenters = getAllByReasonId(reasonID);
            for (ReasonParameterPresenter reasonParameterPresenter : reasonParameterPresenters) {
                if (worker.get().getWorkerLaborRelationship().getLaborRelationship().getId().equals(reasonParameterPresenter.getLaborRelationshipPresenter().getId())) {
                    if (worker.get().getWorkerLaborRegime().getLaborRegime().getId().equals(reasonParameterPresenter.getLaborRegimePresenter().getId())) {
                        reasonParameterPresenter2 = reasonParameterPresenter;
                    }
                }
            }
        }
        return reasonParameterPresenter2;
    }

    @Override
    public String save(ReasonParameters reasonParameters) {
        String save = "No guardado";
        for (ReasonParameterPresenter reasonParameterPresenter : reasonParameters.getReasonParameters()) {
            ReasonParameter reasonParameter = modelMapper.map(reasonParameterPresenter, ReasonParameter.class);
            reasonParameter.setReason(modelMapper.map(reasonParameterPresenter.getReasonPresenter(), Reason.class));
            reasonParameter.setLaborRegime(modelMapper.map(reasonParameterPresenter.getLaborRegimePresenter(), LaborRegime.class));
            reasonParameter.setLaborRelationship(modelMapper.map(reasonParameterPresenter.getLaborRelationshipPresenter(), LaborRelationship.class));

            reasonParameterRepository.save(reasonParameter);
            save = "Guardado";
        }
        return save;
    }

}
