package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.presenter.LaborRelationshipPresenter;

import java.util.List;

public interface LaborRelationshipService {
    List<LaborRelationshipPresenter> getAll();
}
