package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.entity.*;
import ec.edu.espoch.PermissionsDA.presenter.DocumentPresenter;
import ec.edu.espoch.PermissionsDA.presenter.PermissionLogPresenter;
import ec.edu.espoch.PermissionsDA.presenter.PermissionPresenter;
import ec.edu.espoch.PermissionsDA.presenter.PermissionTimePresenter;
import ec.edu.espoch.PermissionsDA.repository.*;
import ec.edu.espoch.PermissionsDA.service.PermissionPropertiesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class PermissionPropertiesServiceImpl implements PermissionPropertiesService {
    @Autowired
    private WorkerRepository workerRepository;
    @Autowired
    private ReasonRepository reasonRepository;
    @Autowired
    private PermissionStateRepository permissionStateRepository;

    @Autowired
    private PermissionTimeRepository permissionTimeRepository;

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private PermissionLogRepository permissionLogRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private WorkerLaborRelationshipRepository workerLaborRelationshipRepository;

    @Override
    public Worker getWorkerByDni(String workerDni) {
        return workerRepository.findByDni(workerDni).get();
    }

    @Override
    public Reason getReasonById(Integer reasonId) {
        return reasonRepository.findById(reasonId).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "Reason Not Exist"));
    }


    @Override
    public void savePermissionTimeByPermissionId(Permission permissionToSave, List<PermissionTimePresenter> permissionTimePresenters) {
        permissionTimeRepository.deleteAllByPermissionId(permissionToSave.getId());
        permissionTimePresenters.forEach(permissionTimePresenter -> {
            PermissionTime permissionTime = modelMapper.map(permissionTimePresenter, PermissionTime.class);
            permissionTime.setPermission(permissionToSave);
            permissionTimeRepository.save(permissionTime);
        });
    }

    @Override
    public void saveDocumentByPermissionId(Permission permissionToSave, List<DocumentPresenter> documentPresenters) {
        documentRepository.deleteAllByPermissionId(permissionToSave.getId());
        documentPresenters.forEach(documentPresenter -> {
            Document document = modelMapper.map(documentPresenter, Document.class);
            document.setPermission(permissionToSave);
            documentRepository.save(document);
        });
    }

    @Override
    public void savePermissionLogByPermissionId(Permission permissionToSave, List<PermissionLogPresenter> permissionLogPresenters) {
        permissionLogPresenters.forEach(permissionLogPresenter -> {
            PermissionLog permissionLog = modelMapper.map(permissionLogPresenter, PermissionLog.class);
            permissionLog.setPermission(permissionToSave);
            permissionLog.setWorker(permissionToSave.getWorker());
            permissionLogRepository.save(permissionLog);
        });
    }

    @Override
    public PermissionState getStateById(Integer stateId) {
        return permissionStateRepository.findById(stateId).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "State Not Exist"));
    }

    @Override
    public List<PermissionTimePresenter> getPermissionTimePresenters(Integer id) {
        List<PermissionTimePresenter> permissionTimePresenters = new ArrayList<>();
        permissionTimeRepository.findAllByPermissionId(id).forEach(permissionTime -> {
            PermissionTimePresenter permissionTimePresenter = modelMapper.map(permissionTime, PermissionTimePresenter.class);
            permissionTimePresenters.add(permissionTimePresenter);
        });
        return permissionTimePresenters;
    }

    @Override
    public List<DocumentPresenter> getDocumentPresenters(Integer id) {
        List<DocumentPresenter> documentPresenters = new ArrayList<>();
        documentRepository.findAllByPermissionId(id).forEach(document -> {
            DocumentPresenter documentPresenter = modelMapper.map(document, DocumentPresenter.class);
            documentPresenters.add(documentPresenter);
        });
        return documentPresenters;
    }


    @Override
    public List<PermissionLogPresenter> getPermissionLogPresenters(Integer id) {
        List<PermissionLogPresenter> permissionLogPresenters = new ArrayList<>();
        permissionLogRepository.findAllByPermissionId(id).forEach(permissionLog -> {
            PermissionLogPresenter permissionLogPresenter = modelMapper.map(permissionLog, PermissionLogPresenter.class);
            permissionLogPresenters.add(permissionLogPresenter);
        });
        return permissionLogPresenters;
    }

    @Override
    public String validatePermission(PermissionPresenter permissionPresenter) {
        AtomicReference<String> validationMessage = new AtomicReference<>("Valid");
        Optional<Worker> worker = workerRepository.findByDni(permissionPresenter.getWorkerDni());
        if (worker.isPresent()) {
            permissionRepository.findAllByWorkerId(worker.get().getId()).forEach(permission -> {
                int idPresenter = permissionPresenter.getId();
                int idDb = permission.getId();
                if (idPresenter != idDb) {
                    permission.getPermissionTimes().forEach(permissionTime -> {
                        Date initialDateDb = permissionTime.getStartDate();
                        Date endDateDb = permissionTime.getReturnDate();
                        permissionPresenter.getPermissionTimePresenters().forEach(permissionTimePresenter -> {

                            Date initialDate = permissionTimePresenter.getStartDate();
                            Date endDate = permissionTimePresenter.getReturnDate();
                            Date date = initialDate;
                            while ((date.compareTo(initialDate) == 0 || date.compareTo(endDate) == 0) || (date.after(initialDate) && date.before(endDate))) {
                                Calendar calendar = Calendar.getInstance();
                                calendar.setTime(date);
                                if ((date.compareTo(initialDateDb) == 0 || date.compareTo(endDateDb) == 0) || (date.after(initialDateDb) && date.before(endDateDb))) {
                                    validationMessage.set("Ya tiene un permiso entre esas fechas");
                                }
                                calendar.add(Calendar.DAY_OF_YEAR, 1);
                                date = calendar.getTime();
                            }
                        });
                    });
                }
            });
        }
        return validationMessage.get();
    }

    @Override
    public String deletePermissionById(Integer permissionId) {
        Optional<Permission> permission = permissionRepository.findById(permissionId);
        if (permission.isPresent()) {
            if (permission.get().getState().getId() != 11 && permission.get().getState().getId() != 12) {
                try {
                    documentRepository.deleteAllByPermissionId(permissionId);
                    permissionLogRepository.deleteAllByPermissionId(permissionId);
                    permissionTimeRepository.deleteAllByPermissionId(permissionId);
                    permissionRepository.deleteById(permissionId);
                    return "Eliminado";
                } catch (Exception e) {
                    return "No se elimino";
                }

            } else
                return "No se puede elminar un permiso en Curso o Finalizado";
        } else
            return "No existe el permiso";
    }
}
