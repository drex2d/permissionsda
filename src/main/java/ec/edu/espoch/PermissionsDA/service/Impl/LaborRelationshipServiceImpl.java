package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.presenter.LaborRelationshipPresenter;
import ec.edu.espoch.PermissionsDA.repository.LaborRelationshipRepository;
import ec.edu.espoch.PermissionsDA.service.LaborRelationshipService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LaborRelationshipServiceImpl implements LaborRelationshipService {
    @Autowired
    private LaborRelationshipRepository laborRelationshipRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<LaborRelationshipPresenter> getAll() {
        List<LaborRelationshipPresenter> laborRelationshipPresenters = new ArrayList<>();
        laborRelationshipRepository.findAll().forEach(laborRelationship -> {
            LaborRelationshipPresenter laborRelationshipPresenter = modelMapper.map(laborRelationship, LaborRelationshipPresenter.class);
            laborRelationshipPresenters.add(laborRelationshipPresenter);
        });
        return laborRelationshipPresenters;
    }
}
