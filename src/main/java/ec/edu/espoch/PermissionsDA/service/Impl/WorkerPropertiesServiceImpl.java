package ec.edu.espoch.PermissionsDA.service.Impl;


import ec.edu.espoch.PermissionsDA.entity.*;
import ec.edu.espoch.PermissionsDA.presenter.*;
import ec.edu.espoch.PermissionsDA.repository.RolRepository;
import ec.edu.espoch.PermissionsDA.repository.WorkerLaborRegimeRepository;
import ec.edu.espoch.PermissionsDA.repository.WorkerLaborRelationshipRepository;
import ec.edu.espoch.PermissionsDA.repository.WorkerRolRepository;
import ec.edu.espoch.PermissionsDA.service.WorkerPropertiesService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class WorkerPropertiesServiceImpl implements WorkerPropertiesService {
    @Autowired
    private WorkerRolRepository workerRolRepository;

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private WorkerLaborRelationshipRepository workerLaborRelationshipRepository;

    @Autowired
    private WorkerLaborRegimeRepository workerLaborRegimeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public void saveRolesByWorker(Integer workerId, Worker worker, List<RolPresenter> rolPresenters) {
        if (workerId != null) {
            workerRolRepository.deleteByWorkerId(workerId);
        }
        rolPresenters.forEach(rolPresenter -> workerRolRepository.save(WorkerRol.builder()
                .worker(worker)
                .rol(rolRepository.findById(rolPresenter.getId()).orElseThrow(() -> new ResponseStatusException(HttpStatus.PRECONDITION_FAILED, "Rol No Exists")))
                .build()));
    }

    @Override
    public void saveWorkerLaborRelationshipByWorker(Integer workerId, Worker worker, WorkerLaborRelationshipPresenter workerLaborRelationshipPresenter) {
//        if (workerId != null) {
        workerLaborRelationshipRepository.deleteAllByWorkerId(workerId);
//        }
        workerLaborRelationshipRepository.save(WorkerLaborRelationship.builder()
                .worker(worker)
                .laborRelationship(modelMapper.map(workerLaborRelationshipPresenter.getLaborRelationshipPresenter(), LaborRelationship.class))
                .contractEndDate(workerLaborRelationshipPresenter.getContractEndDate())
                .contractStartDate(workerLaborRelationshipPresenter.getContractStartDate())
                .endTime(workerLaborRelationshipPresenter.getEndTime())
                .startTime(workerLaborRelationshipPresenter.getStartTime())
                .build());
    }

    @Override
    public void saveWorkerLaborRegimeByWorker(Integer workerId, Worker worker, WorkerLaborRegimePresenter workerLaborRegimePresenter) {
        if (workerId != null) {
            workerLaborRegimeRepository.deleteAllByWorkerId(workerId);
        }
        workerLaborRegimeRepository.save(WorkerLaborRegime.builder()
                .worker(worker)
                .laborRegime(modelMapper.map(workerLaborRegimePresenter.getLaborRegimePresenter(), LaborRegime.class))
                .jobTitle(workerLaborRegimePresenter.getJobTitle())
                .workDependency(workerLaborRegimePresenter.getWorkDependency())
                .build());
    }

    @Override
    public List<RolPresenter> getRolesByWorker(String dni) {
        //Dni para solicitar roles del trabajador del servicio web Espoch
        List<Integer> rolesCas = Arrays.asList(1, 2, 3, 4);
        List<RolPresenter> rolPresenters = new ArrayList<>();
        rolesCas.forEach(rolCas -> {
            RolPresenter rolPresenter = new RolPresenter();
            rolPresenter.setId(rolCas);
            rolPresenters.add(rolPresenter);
        });
        return rolPresenters;
    }

    @Override
    public WorkerLaborRelationshipPresenter getWorkerLaborRelationshipByWorker(String dni) {
        //Dni para solicitar relacionLaboral del trabajador del servicio web Espoch
        Integer laborRelationshipE = 1;

        Date contractStartDate = new Date(2021 - 1900, 0, 01);
        System.out.println(contractStartDate.getYear() + "year");
        System.out.println(contractStartDate.getYear());
        Date contractEndDate = new Date(2022 - 1900, 0, 01);

        Date startTime = new Date(2020 - 1900, 02, 15, 8, 00);

        Date endTime = new Date(2020 - 1900, 02, 15, 17, 00);

        LaborRelationshipPresenter laborRelationshipPresenter = new LaborRelationshipPresenter();
        laborRelationshipPresenter.setId(laborRelationshipE);

        WorkerLaborRelationshipPresenter workerLaborRelationshipPresenter = new WorkerLaborRelationshipPresenter();
        workerLaborRelationshipPresenter.setLaborRelationshipPresenter(laborRelationshipPresenter);

        workerLaborRelationshipPresenter.setContractStartDate(contractStartDate);
        workerLaborRelationshipPresenter.setContractEndDate(contractEndDate);
        workerLaborRelationshipPresenter.setStartTime(startTime);
        workerLaborRelationshipPresenter.setEndTime(endTime);

        return workerLaborRelationshipPresenter;
    }

    @Override
    public WorkerLaborRegimePresenter getWorkerLaborRegimeByWorker(String dni) {
        //Dni para solicitar regimenLaboral del trabajador del servicio web Espoch
        Integer laborRegimeE = 2;
        String jobTitle = "Decano";
        String workDependency = "FADE";
        LaborRegimePresenter laborRegimePresenter = new LaborRegimePresenter();
        laborRegimePresenter.setId(laborRegimeE);
        WorkerLaborRegimePresenter workerLaborRegimePresenter = new WorkerLaborRegimePresenter();
        workerLaborRegimePresenter.setLaborRegimePresenter(laborRegimePresenter);
        workerLaborRegimePresenter.setJobTitle(jobTitle);
        workerLaborRegimePresenter.setWorkDependency(workDependency);

        return workerLaborRegimePresenter;
    }

}