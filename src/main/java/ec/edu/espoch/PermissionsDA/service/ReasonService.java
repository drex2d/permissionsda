package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.presenter.ReasonPresenter;

import java.util.List;

public interface ReasonService {
    List<ReasonPresenter> getAllByIdPermissionType(Integer idPermissionType);

    List<ReasonPresenter> getAll();
}
