package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.entity.*;
import ec.edu.espoch.PermissionsDA.presenter.*;

import java.util.List;

public interface PermissionPropertiesService {
    Worker getWorkerByDni(String workerDni);

    Reason getReasonById(Integer reasonId);

    PermissionState getStateById(Integer stateId);

    void savePermissionTimeByPermissionId(Permission permissionToSave, List<PermissionTimePresenter> permissionTimePresenters);

    List<PermissionTimePresenter> getPermissionTimePresenters(Integer id);

    void saveDocumentByPermissionId(Permission permissionToSave, List<DocumentPresenter> documentPresenters);

    List<DocumentPresenter> getDocumentPresenters(Integer id);

    void savePermissionLogByPermissionId(Permission permissionToSave, List<PermissionLogPresenter> permissionLogPresenters);

    List<PermissionLogPresenter> getPermissionLogPresenters(Integer id);

    String validatePermission(PermissionPresenter permissionPresenter);

    String deletePermissionById(Integer permissionId);
}
