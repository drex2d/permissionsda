package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.entity.Worker;
import ec.edu.espoch.PermissionsDA.entity.WorkerRol;
import ec.edu.espoch.PermissionsDA.presenter.RolPresenter;
import ec.edu.espoch.PermissionsDA.presenter.WorkerLaborRegimePresenter;
import ec.edu.espoch.PermissionsDA.presenter.WorkerLaborRelationshipPresenter;

import java.util.List;

public interface WorkerPropertiesService {
    void saveRolesByWorker(Integer workerId, Worker worker, List<RolPresenter> rolPresenters);

    void saveWorkerLaborRelationshipByWorker(Integer workerId, Worker worker, WorkerLaborRelationshipPresenter workerLaborRelationshipPresenter);

    void saveWorkerLaborRegimeByWorker(Integer workerId, Worker worker, WorkerLaborRegimePresenter workerLaborRegimePresenter);

    List<RolPresenter> getRolesByWorker(String dni);

    WorkerLaborRelationshipPresenter getWorkerLaborRelationshipByWorker(String dni);

    WorkerLaborRegimePresenter getWorkerLaborRegimeByWorker(String dni);
}