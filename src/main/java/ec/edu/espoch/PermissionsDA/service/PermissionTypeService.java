package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.entity.Reason;
import ec.edu.espoch.PermissionsDA.presenter.PermissionTypePresenter;

import java.util.List;

public interface PermissionTypeService {
    List<PermissionTypePresenter> getAll();
}
