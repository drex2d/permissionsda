package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.entity.*;
import ec.edu.espoch.PermissionsDA.presenter.*;
import ec.edu.espoch.PermissionsDA.repository.PermissionLogRepository;
import ec.edu.espoch.PermissionsDA.repository.PermissionRepository;
import ec.edu.espoch.PermissionsDA.repository.PermissionStateRepository;
import ec.edu.espoch.PermissionsDA.repository.PermissionTimeRepository;
import ec.edu.espoch.PermissionsDA.service.PermissionPropertiesService;
import ec.edu.espoch.PermissionsDA.service.PermissionService;
import ec.edu.espoch.PermissionsDA.service.ReasonParameterService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private PermissionRepository permissionRepository;

    @Autowired
    private PermissionPropertiesService permissionPropertiesService;

    @Autowired
    private PermissionTimeRepository permissionTimeRepository;

    @Autowired
    private ReasonParameterService reasonParameterService;

    @Autowired
    private PermissionStateRepository permissionStateRepository;

    @Autowired
    private PermissionLogRepository permissionLogRepository;

    @Override
    @Transactional
    public String save(PermissionPresenter permissionPresenter) {
        String validate = permissionPropertiesService.validatePermission(permissionPresenter);
        String permissionsId;

        if (validate.equals("Valid")) {
            Permission permissionToSave = modelMapper.map(permissionPresenter, Permission.class);
            if (permissionToSave.getId() == -1)
                permissionToSave.setId(null);
            permissionToSave.setWorker(permissionPropertiesService.getWorkerByDni(permissionPresenter.getWorkerDni()));
            permissionToSave.setReason(modelMapper.map(permissionPresenter.getReasonPresenter(), Reason.class));
            permissionToSave.setState(modelMapper.map(permissionPresenter.getPermissionStatePresenter(), PermissionState.class));
            permissionRepository.save(permissionToSave);
            permissionPropertiesService.savePermissionTimeByPermissionId(permissionToSave, permissionPresenter.getPermissionTimePresenters());

            for (PermissionLog log : permissionLogRepository.findAllByPermissionId(permissionToSave.getId())) {
                if (log.getDescription().equals("Atrasado") || log.getDescription().equals("Planificacion") || log.getDescription().contains("ComentarioT") || log.getDescription().equals("Multiples") || log.getDescription().equals("Prematuro") || log.getDescription().equals("Discapacidad") || log.getDescription().equals("Fallecida")) {
                    permissionLogRepository.delete(log);
                }
            }
            permissionPropertiesService.savePermissionLogByPermissionId(permissionToSave, permissionPresenter.getPermissionLogPresenters());
            if (permissionPresenter.getDocumentPresenters() != null) {
                permissionPropertiesService.saveDocumentByPermissionId(permissionToSave, permissionPresenter.getDocumentPresenters());
            }
//        if (permissionPresenter.getPermissionLogPresenters() != null) {
//            permissionPropertiesService.savePermissionLogByPermissionId(permissionToSave, permissionPresenter.getPermissionLogPresenters());
//        }
            permissionsId = permissionToSave.getId().toString();
        } else
            permissionsId = validate;
        return permissionsId;
    }


    @Override
    @Transactional
    public List<PermissionPresenter> getAll() {
        List<PermissionPresenter> permissionPresenters = new ArrayList<>();
        permissionRepository.findAll().forEach(permission -> {
            PermissionPresenter permissionPresenter = assignPermissionData(permission);

            permissionPresenters.add(permissionPresenter);
        });
        return permissionPresenters;
    }

    @Override
    @Transactional
    public List<PermissionPresenter> getAllByWorkerDni(String dni) {
        Worker worker = permissionPropertiesService.getWorkerByDni(dni);
        List<PermissionPresenter> permissionPresenters = new ArrayList<>();
        permissionRepository.findAllByWorkerId(worker.getId()).forEach(permission -> {
            PermissionPresenter permissionPresenter = assignPermissionData(permission);

            permissionPresenters.add(permissionPresenter);
        });
        return permissionPresenters;
    }

    @Override
    @Transactional
    public List<PermissionPresenter> getAllByReasonId(Integer reasonId) {
        List<PermissionPresenter> permissionPresenters = new ArrayList<>();
        permissionRepository.findAllByReasonId(reasonId).forEach(permission -> {
            PermissionPresenter permissionPresenter = assignPermissionData(permission);

            permissionPresenters.add(permissionPresenter);
        });
        return permissionPresenters;
    }

    @Override
    @Transactional
    public List<PermissionPresenter> getAllByStateId(Integer stateId) {
        List<PermissionPresenter> permissionPresenters = new ArrayList<>();
        permissionRepository.findAllByStateId(stateId).forEach(permission -> {
            PermissionPresenter permissionPresenter = assignPermissionData(permission);

            permissionPresenters.add(permissionPresenter);
        });
        return permissionPresenters;
    }

    @Override
    @Transactional
    public List<PermissionPresenter> getAllByStateAndDependency(Integer stateId, String workDependency) {
        List<PermissionPresenter> permissionPresentersByStateIdAndDependency = new ArrayList<>();
        permissionRepository.findAllByStateId(stateId).forEach(permission -> {
            if (permission.getWorker().getWorkerLaborRegime().getWorkDependency().equals(workDependency)) {
                PermissionPresenter permissionPresenter = assignPermissionData(permission);

                permissionPresentersByStateIdAndDependency.add(permissionPresenter);
            }
        });
        return permissionPresentersByStateIdAndDependency;
    }

    @Override
    @Transactional
    public List<PermissionPresenter> getAlByWorkerDniAndPermissionTypeId(String workerDni, Integer permissionTypeId) {
        List<PermissionPresenter> permissionPresentersByWorkerDniAndPermissionTypeId = new ArrayList<>();

        Worker worker = permissionPropertiesService.getWorkerByDni(workerDni);
        permissionRepository.findAllByWorkerId(worker.getId()).forEach(permission -> {
            if (permission.getReason().getPermissionType().getId().equals(permissionTypeId)) {
                PermissionPresenter permissionPresenter = assignPermissionData(permission);

                permissionPresentersByWorkerDniAndPermissionTypeId.add(permissionPresenter);
            }
        });
        return permissionPresentersByWorkerDniAndPermissionTypeId;
    }

    @Override
    public PermissionPresenter getByPermissionId(Integer permissionId) {
        PermissionPresenter permissionPresenter = new PermissionPresenter();
        Optional<Permission> permissionOptional = permissionRepository.findById(permissionId);
        if (permissionOptional.isPresent()) {
            Permission permission = permissionOptional.get();
            permissionPresenter = assignPermissionData(permission);
        }
        return permissionPresenter;
    }

    @Override
    @Transactional
    public String deleteById(Integer permissionId) {
        return permissionPropertiesService.deletePermissionById(permissionId);
    }

    @Override
    public List<PermissionPresenter> getAllByWorkerDniAndStateId(String workerDni, Integer stateId) {
        Worker worker = permissionPropertiesService.getWorkerByDni(workerDni);
        List<Permission> permissions = permissionRepository.findAllByWorkerId(worker.getId());
        List<PermissionPresenter> permissionPresenters = new ArrayList<>();
        permissions.forEach(permission -> {
            if (permission.getState().getId() == stateId) {
                permissionPresenters.add(assignPermissionData(permission));
            }
        });
        return permissionPresenters;
    }

    @Override
    public String changeStatePermission(PermissionsIdsPresenter permissionsIdsPresenter) {
        String resp = "noChange";
        for (Integer permissionId : permissionsIdsPresenter.getListPermissionsId()) {
            Optional<Permission> optionalPermission = permissionRepository.findById(permissionId);
            if (optionalPermission.isPresent()) {
                Permission permission = optionalPermission.get();
                Optional<PermissionState> optionalPermissionState = permissionStateRepository.findById(permissionsIdsPresenter.getStateId());

                if (optionalPermissionState.isPresent()) {
                    PermissionState permissionState = optionalPermissionState.get();
                    PermissionLog permissionLog = new PermissionLog();
//                permissionState=optionalPermissionState.get();
                    permissionLog.setPermission(permission);

                    permissionLog.setWorker(permissionPropertiesService.getWorkerByDni(permissionsIdsPresenter.getWorkerDni()));
                    permissionLog.setLogDate(new Date());
                    permissionLog.setDescription(permissionState.getDescription());
                    permission.getPermissionLogs().add(permissionLog);
                    permission.setState(permissionState);
                    permissionRepository.save(permission);
                    resp = "change";
                }
            }
        }
        return resp;
    }

    @Override
    @Transactional
    public void updatePermissions() {
        permissionRepository.findAllByStateId(4).forEach(permission -> {
            permission.getPermissionTimes().forEach(permissionTime -> {
                Date today = new Date();
                if ((today.compareTo(permissionTime.getStartDate()) == 0 || today.compareTo(permissionTime.getReturnDate()) == 0) || today.after(permissionTime.getStartDate()) && today.before(permissionTime.getReturnDate())) {
                    PermissionState permissionState = new PermissionState();
                    System.out.println("En curs");
                    permissionState.setId(12);
                    permission.setState(permissionState);
                    permissionRepository.save(permission);
                }
            });
        });

        permissionRepository.findAllByStateId(12).forEach(permission -> {
            permission.getPermissionTimes().forEach(permissionTime -> {
                Date today = new Date();
                if (today.after(permissionTime.getReturnDate())) {
                    PermissionState permissionState = new PermissionState();
                    System.out.println("Finished");
                    permissionState.setId(11);
                    permission.setState(permissionState);
                    permissionRepository.save(permission);
                }
            });
        });

    }

    @Override
    public List<PermissionPresenter> getAllNotificationsByWorkerDni(String workerDni) {
        List<PermissionPresenter> permissionPresenters = new ArrayList<>();
        getAllByWorkerDni(workerDni).forEach(permissionPresenter -> {
            if (permissionPresenter.getPermissionStatePresenter().getId() >= 2 && permissionPresenter.getPermissionStatePresenter().getId() <= 4) {
                permissionPresenters.add(permissionPresenter);
            }
        });
        return permissionPresenters;
    }

    public PermissionPresenter assignPermissionData(Permission permission) {
        PermissionPresenter permissionPresenter = assignPermissionWorkerData(permission);
        permissionPresenter.setPermissionTimePresenters(permissionPropertiesService.getPermissionTimePresenters(permission.getId()));
        permissionPresenter.setDocumentPresenters(permissionPropertiesService.getDocumentPresenters(permission.getId()));
        permissionPresenter.setPermissionLogPresenters(permissionPropertiesService.getPermissionLogPresenters(permission.getId()));
        return permissionPresenter;
    }

    public PermissionPresenter assignPermissionWorkerData(Permission permission) {
        PermissionPresenter permissionPresenter = modelMapper.map(permission, PermissionPresenter.class);
        permissionPresenter.setReasonPresenter(modelMapper.map(permission.getReason(), ReasonPresenter.class));
        permissionPresenter.setReasonParameterPresenter(modelMapper.map(reasonParameterService.getByReasonIdAndWorkerDni(permission.getReason().getId(), permission.getWorker().getDni()), ReasonParameterPresenter.class));
        permissionPresenter.getReasonPresenter().setPermissionTypePresenter(modelMapper.map(permission.getReason().getPermissionType(), PermissionTypePresenter.class));
        permissionPresenter.setPermissionStatePresenter(modelMapper.map(permission.getState(), PermissionStatePresenter.class));
        permissionPresenter.setWorkerDni(permission.getWorker().getDni());
        permissionPresenter.setWorkerNames(permission.getWorker().getNames());
        permissionPresenter.setWorkerLastNames(permission.getWorker().getLastNames());
        return permissionPresenter;
    }
}
