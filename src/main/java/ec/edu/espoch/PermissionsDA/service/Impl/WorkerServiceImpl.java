package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.entity.Worker;
import ec.edu.espoch.PermissionsDA.entity.WorkerVacation;
import ec.edu.espoch.PermissionsDA.presenter.*;
import ec.edu.espoch.PermissionsDA.repository.WorkerRepository;
import ec.edu.espoch.PermissionsDA.repository.WorkerVacationRepository;
import ec.edu.espoch.PermissionsDA.service.WorkerPropertiesService;
import ec.edu.espoch.PermissionsDA.service.WorkerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class WorkerServiceImpl implements WorkerService {
    @Autowired
    private WorkerRepository workerRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private WorkerPropertiesService workerPropertiesService;

    @Autowired
    private WorkerVacationRepository workerVacationRepository;

    @Override
    @Transactional
    public WorkerPresenter save(WorkerPresenter workerPresenter) {
        Optional<Worker> worker = workerRepository.findByDni(workerPresenter.getDni());
        Worker toSave;
        Integer workerId = null;
        if (worker.isPresent()) {
            workerId = worker.get().getId();
            toSave = worker.get();
            toSave.setNames(workerPresenter.getNames());
            toSave.setLastNames(workerPresenter.getLastNames());
            toSave.setState(workerPresenter.getState());
        } else {
            toSave = modelMapper.map(workerPresenter, Worker.class);
            if (workerPresenter.getId() == null) {
                Date currentDate = new Date();
                WorkerVacation workerVacation = WorkerVacation.builder()
                        .worker(toSave)
                        .accumulatedDays(0.0)
                        .lastAccumulationDate(currentDate)
                        .accumulationYear(currentDate.getYear() + 1900)
                        .state("In Progress")
                        .build();
                workerVacationRepository.save(workerVacation);
            }
        }
        workerRepository.save(toSave);

        workerPresenter.setId(toSave.getId());
        workerPresenter = getEspochWorkerData(workerPresenter);

        workerPropertiesService.saveRolesByWorker(workerId, toSave, workerPresenter.getRolPresenters());
        workerPropertiesService.saveWorkerLaborRelationshipByWorker(workerId, toSave, workerPresenter.getWorkerLaborRelationshipPresenter());
        workerPropertiesService.saveWorkerLaborRegimeByWorker(workerId, toSave, workerPresenter.getWorkerLaborRegimePresenter());

        return WorkerPresenter.builder().id(toSave.getId()).build();

    }

    private WorkerPresenter getEspochWorkerData(WorkerPresenter workerPresenter) {

        workerPresenter.setRolPresenters(workerPropertiesService.getRolesByWorker(workerPresenter.getDni()));
        workerPresenter.setWorkerLaborRelationshipPresenter(workerPropertiesService.getWorkerLaborRelationshipByWorker(workerPresenter.getDni()));
        workerPresenter.setWorkerLaborRegimePresenter(workerPropertiesService.getWorkerLaborRegimeByWorker(workerPresenter.getDni()));
        return workerPresenter;
    }

    @Override
    @Transactional
    public List<WorkerPresenter> getAll() {
        List<WorkerPresenter> workerPresenters = new ArrayList<>();
        workerRepository.findAll().forEach(worker -> {
            WorkerPresenter workerPresenter = modelMapper.map(worker, WorkerPresenter.class);

            worker.getWorkerRoles().forEach(workerRol -> workerPresenter.getRolPresenters().add(modelMapper.map(workerRol.getRol(), RolPresenter.class)));

            workerPresenter.setWorkerLaborRelationshipPresenter(modelMapper.map(worker.getWorkerLaborRelationship(), WorkerLaborRelationshipPresenter.class));
            workerPresenter.getWorkerLaborRelationshipPresenter().setLaborRelationshipPresenter(modelMapper.map(worker.getWorkerLaborRelationship().getLaborRelationship(), LaborRelationshipPresenter.class));
            workerPresenter.setWorkerLaborRegimePresenter(modelMapper.map(worker.getWorkerLaborRegime(), WorkerLaborRegimePresenter.class));
            workerPresenter.getWorkerLaborRegimePresenter().setLaborRegimePresenter(modelMapper.map(worker.getWorkerLaborRegime().getLaborRegime(), LaborRegimePresenter.class));
            worker.getWorkerVacations().forEach(workerVacation -> workerPresenter.getWorkerVacationPresenters().add(modelMapper.map(workerVacation, WorkerVacationPresenter.class)));
            workerPresenters.add(workerPresenter);
        });
        return workerPresenters;
    }

    @Override
    public WorkerPresenter fetchWorkerByDni(String dni) {
        Optional<Worker> worker = workerRepository.findByDni(dni);
        return this.transformEntityToPresenter(worker);
    }

    private WorkerPresenter transformEntityToPresenter(Optional<Worker> worker) {
        if (worker.isPresent()) {
            WorkerPresenter workerPresenter = modelMapper.map(worker.get(), WorkerPresenter.class);
            worker.get().getWorkerRoles().forEach(workerRol -> workerPresenter.getRolPresenters().add(modelMapper.map(workerRol.getRol(), RolPresenter.class)));
            workerPresenter.setWorkerLaborRelationshipPresenter(modelMapper.map(worker.get().getWorkerLaborRelationship(), WorkerLaborRelationshipPresenter.class));
            workerPresenter.getWorkerLaborRelationshipPresenter().setLaborRelationshipPresenter(modelMapper.map(worker.get().getWorkerLaborRelationship().getLaborRelationship(), LaborRelationshipPresenter.class));
            workerPresenter.setWorkerLaborRegimePresenter(modelMapper.map(worker.get().getWorkerLaborRegime(), WorkerLaborRegimePresenter.class));
            workerPresenter.getWorkerLaborRegimePresenter().setLaborRegimePresenter(modelMapper.map(worker.get().getWorkerLaborRegime().getLaborRegime(), LaborRegimePresenter.class));
            worker.get().getWorkerVacations().forEach(workerVacation -> workerPresenter.getWorkerVacationPresenters().add(modelMapper.map(workerVacation, WorkerVacationPresenter.class)));
            return workerPresenter;
        } else
            return null;
    }

    @Override
    public WorkerPresenter fetchWorkerById(Integer id) {
        Optional<Worker> worker = workerRepository.findById(id);
        return this.transformEntityToPresenter(worker);
    }

    @Override
    @Transactional
    public void delete(String dni) {
        Optional<Worker> worker = workerRepository.findByDni(dni);
        workerRepository.deleteById(worker.get().getId());
    }

    @Override
    public List<WorkerPresenter> searchAllByFilter(String filter) {
        List<WorkerPresenter> workerPresenters = new ArrayList<>();
        workerRepository.findWorkerByKeyWord(filter).forEach(worker -> {
            WorkerPresenter workerPresenter = modelMapper.map(worker, WorkerPresenter.class);

            worker.getWorkerRoles().forEach(workerRol -> workerPresenter.getRolPresenters().add(modelMapper.map(workerRol.getRol(), RolPresenter.class)));
            workerPresenter.setWorkerLaborRelationshipPresenter(modelMapper.map(worker.getWorkerLaborRelationship(), WorkerLaborRelationshipPresenter.class));
            workerPresenter.getWorkerLaborRelationshipPresenter().setLaborRelationshipPresenter(modelMapper.map(worker.getWorkerLaborRelationship().getLaborRelationship(), LaborRelationshipPresenter.class));
            workerPresenter.setWorkerLaborRegimePresenter(modelMapper.map(worker.getWorkerLaborRegime(), WorkerLaborRegimePresenter.class));
            workerPresenter.getWorkerLaborRegimePresenter().setLaborRegimePresenter(modelMapper.map(worker.getWorkerLaborRegime().getLaborRegime(), LaborRegimePresenter.class));
            worker.getWorkerVacations().forEach(workerVacation -> workerPresenter.getWorkerVacationPresenters().add(modelMapper.map(workerVacation, WorkerVacationPresenter.class)));

            workerPresenters.add(workerPresenter);
        });

        return workerPresenters;
    }
}