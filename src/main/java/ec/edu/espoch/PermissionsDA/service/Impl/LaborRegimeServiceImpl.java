package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.presenter.LaborRegimePresenter;
import ec.edu.espoch.PermissionsDA.repository.LaborRegimeRepository;
import ec.edu.espoch.PermissionsDA.service.LaborRegimeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LaborRegimeServiceImpl implements LaborRegimeService {

    @Autowired
    private LaborRegimeRepository laborRegimeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<LaborRegimePresenter> getAll() {
        List<LaborRegimePresenter> laborRegimePresenters = new ArrayList<>();
        laborRegimeRepository.findAll().forEach(laborRegime -> {
            LaborRegimePresenter laborRegimePresenter = modelMapper.map(laborRegime, LaborRegimePresenter.class);
            laborRegimePresenters.add(laborRegimePresenter);
        });
        return laborRegimePresenters;
    }
}
