package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.presenter.ReasonParameterPresenter;
import ec.edu.espoch.PermissionsDA.presenter.ReasonParameters;

import java.util.List;

public interface ReasonParameterService {
    List<ReasonParameterPresenter> getAllByReasonId(Integer idReason);

    ReasonParameterPresenter getByReasonIdAndWorkerDni(Integer reasonID, String workerDni);

    String save(ReasonParameters reasonParametersPresenter);
}
