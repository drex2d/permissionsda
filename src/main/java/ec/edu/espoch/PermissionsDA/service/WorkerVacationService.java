package ec.edu.espoch.PermissionsDA.service;

import ec.edu.espoch.PermissionsDA.entity.Worker;

public interface WorkerVacationService {

    void updateAccumulation();
}
