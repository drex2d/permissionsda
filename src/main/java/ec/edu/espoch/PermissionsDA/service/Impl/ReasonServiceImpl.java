package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.presenter.PermissionTypePresenter;
import ec.edu.espoch.PermissionsDA.presenter.ReasonPresenter;
import ec.edu.espoch.PermissionsDA.repository.ReasonRepository;
import ec.edu.espoch.PermissionsDA.service.ReasonService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReasonServiceImpl implements ReasonService {
    @Autowired
    private ReasonRepository reasonRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public List<ReasonPresenter> getAllByIdPermissionType(Integer idPermissionType) {
        List<ReasonPresenter> reasonPresenters = new ArrayList<>();

        reasonRepository.findAllByPermissionTypeId(idPermissionType).forEach(reason -> {
            ReasonPresenter reasonPresenter = modelMapper.map(reason, ReasonPresenter.class);
            reasonPresenter.setPermissionTypePresenter(modelMapper.map(reason.getPermissionType(), PermissionTypePresenter.class));
            reasonPresenters.add(reasonPresenter);
        });
        return reasonPresenters;
    }

    @Override
    public List<ReasonPresenter> getAll() {
        List<ReasonPresenter> reasonPresenters = new ArrayList<>();

        reasonRepository.findAll().forEach(reason -> {
            ReasonPresenter reasonPresenter = modelMapper.map(reason, ReasonPresenter.class);
            reasonPresenter.setPermissionTypePresenter(modelMapper.map(reason.getPermissionType(), PermissionTypePresenter.class));
            reasonPresenters.add(reasonPresenter);
        });
        return reasonPresenters;
    }
}
