package ec.edu.espoch.PermissionsDA.service.Impl;

import ec.edu.espoch.PermissionsDA.presenter.PermissionTypePresenter;
import ec.edu.espoch.PermissionsDA.repository.PermissionTypeRepository;
import ec.edu.espoch.PermissionsDA.service.PermissionTypeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class PermissionTypeServiceImpl implements PermissionTypeService {

    @Autowired
    private PermissionTypeRepository permissionTypeRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    @Transactional
    public List<PermissionTypePresenter> getAll() {
        List<PermissionTypePresenter> permissionTypePresenters = new ArrayList<>();
        permissionTypeRepository.findAll().forEach(permissionType -> {
            PermissionTypePresenter permissionTypePresenter = modelMapper.map(permissionType, PermissionTypePresenter.class);
            permissionTypePresenters.add(permissionTypePresenter);
        });
        return permissionTypePresenters;
    }
}
