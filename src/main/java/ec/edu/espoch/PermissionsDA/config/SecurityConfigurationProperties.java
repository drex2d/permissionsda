package ec.edu.espoch.PermissionsDA.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
@ConfigurationProperties("app.config.security.cors")
public class SecurityConfigurationProperties {
    private List<String> allowedOrigins;

}
