package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.ReasonParameter;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReasonParameterRepository extends CrudRepository<ReasonParameter, Integer> {
    List<ReasonParameter> findAllByReasonId(@Param("reasonId") Integer ReasonId);
}
