package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.WorkerLaborRelationship;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerLaborRelationshipRepository extends CrudRepository<WorkerLaborRelationship, Integer> {
    @Modifying
    @Query("delete from WorkerLaborRelationship wr where wr.worker.id = :id")
    void deleteAllByWorkerId(@Param("id") Integer id);

    WorkerLaborRelationship findByWorkerId(@Param("workerId") Integer workerId);
}
