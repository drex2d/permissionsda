package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.WorkerVacation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerVacationRepository extends CrudRepository<WorkerVacation, Integer> {
}
