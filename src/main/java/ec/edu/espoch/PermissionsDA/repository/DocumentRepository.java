package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.Document;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Integer> {
    void deleteAllByPermissionId(Integer permissionId);

    List<Document> findAllByPermissionId(@Param("permissionId") Integer permissionId);
}
