package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.Reason;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReasonRepository extends CrudRepository<Reason, Integer> {
    List<Reason> findAllByPermissionTypeId(@Param("permissionTypeId") Integer permissionTypeId);
}
