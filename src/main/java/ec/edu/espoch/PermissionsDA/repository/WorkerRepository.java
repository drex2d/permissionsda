package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.Worker;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WorkerRepository extends CrudRepository<Worker, Integer> {
    Optional<Worker> findByDni(@Param("dni") String dni);

    @Query(value = "SELECT * FROM Worker w WHERE w.dni LIKE %:keyword% or w.names LIKE %:keyword% or w.last_names LIKE %:keyword%", nativeQuery = true)
    List<Worker> findWorkerByKeyWord(@Param("keyword") String keyword);

}