package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.LaborRegime;
import ec.edu.espoch.PermissionsDA.entity.WorkerLaborRegime;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerLaborRegimeRepository extends CrudRepository<WorkerLaborRegime, Integer> {
    @Modifying
    @Query("delete from WorkerLaborRegime wr where wr.worker.id = :id")
    void deleteAllByWorkerId(@Param("id") Integer id);

    WorkerLaborRegime findByWorkerId(@Param("workerId") Integer workerId);

}
