package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.WorkerRol;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerRolRepository extends CrudRepository<WorkerRol, Integer> {
    @Modifying
    @Query("delete from WorkerRol wr where wr.worker.id = :id")
    void deleteByWorkerId(@Param("id") Integer id);
}