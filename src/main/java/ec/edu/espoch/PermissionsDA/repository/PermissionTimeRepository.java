package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.PermissionTime;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionTimeRepository extends CrudRepository<PermissionTime, Integer> {
    void deleteAllByPermissionId(Integer permissionId);

    List<PermissionTime> findAllByPermissionId(@Param("permissionId") Integer permissionId);
}
