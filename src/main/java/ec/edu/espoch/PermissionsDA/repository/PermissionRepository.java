package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.Permission;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PermissionRepository extends CrudRepository<Permission, Integer> {
    List<Permission> findAllByWorkerId(@Param("workerId") Integer workerId);

    List<Permission> findAllByReasonId(@Param("reasonId") Integer reasonId);

    List<Permission> findAllByStateId(@Param("stateId") Integer stateId);
}
