package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.LaborRegime;
import ec.edu.espoch.PermissionsDA.entity.WorkerLaborRegime;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LaborRegimeRepository extends CrudRepository<LaborRegime, Integer> {
}
