package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.PermissionLog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionLogRepository extends CrudRepository<PermissionLog, Integer> {
    List<PermissionLog> findAllByPermissionId(@Param("permissionId") Integer permissionId);

    void deleteAllByPermissionId(Integer permissionId);
}
