package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.PermissionType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionTypeRepository extends CrudRepository<PermissionType, Integer> {
}
