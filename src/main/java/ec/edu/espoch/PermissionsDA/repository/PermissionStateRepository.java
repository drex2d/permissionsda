package ec.edu.espoch.PermissionsDA.repository;

import ec.edu.espoch.PermissionsDA.entity.PermissionState;
import org.springframework.data.repository.CrudRepository;

public interface PermissionStateRepository extends CrudRepository<PermissionState, Integer> {
}
