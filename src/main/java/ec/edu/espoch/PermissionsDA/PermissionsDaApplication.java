package ec.edu.espoch.PermissionsDA;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.TimeZone;

@SpringBootApplication
public class PermissionsDaApplication {

    public static void main(String[] args) {
//        TimeZone.setDefault(TimeZone.getTimeZone("America/Guayaquil"));
        SpringApplication.run(PermissionsDaApplication.class, args);
    }

}