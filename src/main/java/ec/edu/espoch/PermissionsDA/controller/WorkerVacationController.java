package ec.edu.espoch.PermissionsDA.controller;

import ec.edu.espoch.PermissionsDA.service.WorkerVacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("workerVacation")
@EnableScheduling
public class WorkerVacationController {

    @Autowired
    private WorkerVacationService workerVacationService;

    //cron = "0 55 17 * * *"
    //fixedDelay = 50 * 1000
    @Scheduled(cron = "0 55 17 * * *")
    public void updateAccumulation() {
        System.out.println("Update Accumulation " + new Date());

        workerVacationService.updateAccumulation();

    }
}
