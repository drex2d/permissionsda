package ec.edu.espoch.PermissionsDA.controller;

import ec.edu.espoch.PermissionsDA.presenter.ReasonPresenter;
import ec.edu.espoch.PermissionsDA.service.ReasonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("reason")
public class ReasonController {
    @Autowired
    private ReasonService reasonService;

    @GetMapping("/{idPermissionType}")
    public List<ReasonPresenter> getAllByIdPermissionType(@PathVariable("idPermissionType") Integer idPermissionType){
        return reasonService.getAllByIdPermissionType(idPermissionType);
    }

    @GetMapping("/getAll")
    public List<ReasonPresenter> getAll(){
        return reasonService.getAll();
    }
}
