package ec.edu.espoch.PermissionsDA.controller;

import ec.edu.espoch.PermissionsDA.entity.Reason;
import ec.edu.espoch.PermissionsDA.presenter.PermissionTypePresenter;
import ec.edu.espoch.PermissionsDA.service.PermissionTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("permissionType")
public class PermissionTypeController {
    @Autowired
    private PermissionTypeService permissionTypeService;

    @GetMapping
    public List<PermissionTypePresenter> getAll(){
        return permissionTypeService.getAll();
    }

}
