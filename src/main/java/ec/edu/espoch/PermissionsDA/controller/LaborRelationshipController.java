package ec.edu.espoch.PermissionsDA.controller;

import ec.edu.espoch.PermissionsDA.presenter.LaborRelationshipPresenter;
import ec.edu.espoch.PermissionsDA.service.LaborRelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("laborRelationship")
public class LaborRelationshipController {
    @Autowired
    private LaborRelationshipService laborRelationshipService;

    @GetMapping("/getAll")
    public List<LaborRelationshipPresenter> getAll() {
        return laborRelationshipService.getAll();
    }

}
