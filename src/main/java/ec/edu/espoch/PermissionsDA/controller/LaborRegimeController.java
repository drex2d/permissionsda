package ec.edu.espoch.PermissionsDA.controller;

import ec.edu.espoch.PermissionsDA.presenter.LaborRegimePresenter;
import ec.edu.espoch.PermissionsDA.service.LaborRegimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("laborRegime")
public class LaborRegimeController {
    @Autowired
    private LaborRegimeService laborRegimeService;

    @GetMapping("/getAll")
    public List<LaborRegimePresenter> getAll() {
        return laborRegimeService.getAll();
    }
}
