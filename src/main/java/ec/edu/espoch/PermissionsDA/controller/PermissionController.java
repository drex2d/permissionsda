package ec.edu.espoch.PermissionsDA.controller;

import ec.edu.espoch.PermissionsDA.presenter.PermissionPresenter;
import ec.edu.espoch.PermissionsDA.presenter.PermissionsIdsPresenter;
import ec.edu.espoch.PermissionsDA.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("permission")
@EnableScheduling
public class PermissionController {
    @Autowired
    private PermissionService permissionService;

    @GetMapping("/")
    public List<PermissionPresenter> getAll() {
        return permissionService.getAll();
    }

    @PostMapping("/save")
    public String save(@RequestBody PermissionPresenter permissionPresenter) {
        return permissionService.save(permissionPresenter);
    }

    @GetMapping("/byId/{permissionId}")
    public PermissionPresenter getByPermissionId(@PathVariable("permissionId") Integer permissionId) {
        return permissionService.getByPermissionId(permissionId);
    }

    @DeleteMapping("/delete/{permissionId}")
    public String deleteById(@PathVariable("permissionId") Integer permissionId) {
        return permissionService.deleteById(permissionId);
    }

    @GetMapping("/byWorkerDni/{workerDni}")
    public List<PermissionPresenter> getAllByWorkerDni(@PathVariable("workerDni") String workerDni) {
        return permissionService.getAllByWorkerDni(workerDni);
    }

    @GetMapping("/byReasonId/{reasonId}")
    public List<PermissionPresenter> getAllByReasonId(@PathVariable("reasonId") Integer reasonId) {
        return permissionService.getAllByReasonId(reasonId);
    }

    @GetMapping("/byStateId/{StateId}")
    public List<PermissionPresenter> getAllByStateId(@PathVariable("StateId") Integer StateId) {
        return permissionService.getAllByStateId(StateId);
    }

    @GetMapping("/byWorkerDniAndStateId/{workerDni}/{stateId}")
    public List<PermissionPresenter> getAllByWorkerDniAndStateId(@PathVariable Map<String, String> pathVarsMap) {
        String state = pathVarsMap.get("stateId");
        String workerDni = pathVarsMap.get("workerDni");
        Integer stateId = Integer.parseInt(state);
        return permissionService.getAllByWorkerDniAndStateId(workerDni, stateId);
    }

    @GetMapping("/byStateAndDependency/{stateId}/{workDependency}")
    public List<PermissionPresenter> getAllyStateAndDependency(@PathVariable Map<String, String> pathVarsMap) {
        String state = pathVarsMap.get("stateId");
        String workDependency = pathVarsMap.get("workDependency");
        Integer stateId = Integer.parseInt(state);
        return permissionService.getAllByStateAndDependency(stateId, workDependency);
    }

    @GetMapping("/byWorkerDniAndPermissionTypeId/{workerDni}/{permissionTypeId}")
    public List<PermissionPresenter> getAllByWorkerDniAndPermissionTypeId(@PathVariable Map<String, String> pathVarsMap) {
        String workerDni = pathVarsMap.get("workerDni");
        String permissionType = pathVarsMap.get("permissionTypeId");
        Integer permissionTypeId = Integer.parseInt(permissionType);
        return permissionService.getAlByWorkerDniAndPermissionTypeId(workerDni, permissionTypeId);
    }

    @PostMapping("/changeStatePermission")
    public String changeStatePermission(@RequestBody PermissionsIdsPresenter permissionsIdsPresenter) {
        return permissionService.changeStatePermission(permissionsIdsPresenter);
    }

    @Scheduled(fixedDelay = 60 * 60 * 1000)
    public void updatePermissions() {
        System.out.println("Update Permissions " + new Date());
        permissionService.updatePermissions();
    }

    @GetMapping("/notificationsByWorker/{dni}")
    public String getNotificationsByWorker(@PathVariable("dni") String dni) {
        List<PermissionPresenter> permissionPresenters = permissionService.getAllByWorkerDni(dni);
        int countNotifications = 0;
        for (int i = 0; i < permissionPresenters.size(); i++) {
            if (permissionPresenters.get(i).getPermissionStatePresenter().getId() >= 2 && permissionPresenters.get(i).getPermissionStatePresenter().getId() <= 4)
                countNotifications++;
        }
        return String.valueOf(countNotifications);
    }

    @GetMapping("/notificationsByStateId/{stateId}")
    public String getNotificationsByStateId(@PathVariable("stateId") Integer stateId) {
        return String.valueOf(permissionService.getAllByStateId(stateId).size());
    }

    @GetMapping("/notificationsByStateIdAndDependency/{stateId}/{workDependency}")
    public String getNotificationsByStateAndDependency(@PathVariable Map<String, String> pathVarsMap) {
        String state = pathVarsMap.get("stateId");
        String workDependency = pathVarsMap.get("workDependency");
        Integer stateId = Integer.parseInt(state);
        List<PermissionPresenter> permissionPresenters = permissionService.getAllByStateAndDependency(stateId, workDependency);
        int countNotifications = 0;
        for (int i = 0; i < permissionPresenters.size(); i++) {
            countNotifications++;
        }
        return String.valueOf(countNotifications);
    }

    @GetMapping("/notificationsByWorkerDni/{workerDni}")
    public List<PermissionPresenter> getAllNotificationsByWorkerDni(@PathVariable("workerDni") String workerDni) {
        return permissionService.getAllNotificationsByWorkerDni(workerDni);
    }
}