package ec.edu.espoch.PermissionsDA.controller;


import ec.edu.espoch.PermissionsDA.presenter.WorkerPresenter;
import ec.edu.espoch.PermissionsDA.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/worker")
public class WorkerController {
    @Autowired
    private WorkerService workerService;

    @GetMapping("/")
    public List<WorkerPresenter> getAll() {
        return workerService.getAll();
    }

    @GetMapping("/byDni/{dni}")
    public WorkerPresenter workerByDni(@PathVariable("dni") String dni) {
        return workerService.fetchWorkerByDni(dni);
    }

    @GetMapping("/byId/{id}")
    public WorkerPresenter workerById(@PathVariable("id") Integer id) {
        return workerService.fetchWorkerById(id);
    }

    @PostMapping("/save")
    public WorkerPresenter save(@RequestBody WorkerPresenter workerPresenter) {
        return workerService.save(workerPresenter);
    }

    @DeleteMapping("/{dni}")
    public void delete(@PathVariable("dni") String dni) {
        workerService.delete(dni);
    }

    @GetMapping({"/demo"})
    public String hello() {
        return "HelloWorld";
    }

    @GetMapping("/searchAllByFilter/{filter}")
    public List<WorkerPresenter> searchAllByFilter(@PathVariable("filter") String filter) {
        return workerService.searchAllByFilter(filter);
    }

}
