package ec.edu.espoch.PermissionsDA.controller;

import ec.edu.espoch.PermissionsDA.presenter.PermissionsIdsPresenter;
import ec.edu.espoch.PermissionsDA.presenter.ReasonParameterPresenter;
import ec.edu.espoch.PermissionsDA.presenter.ReasonParameters;
import ec.edu.espoch.PermissionsDA.presenter.WorkerPresenter;
import ec.edu.espoch.PermissionsDA.service.ReasonParameterService;
import org.hibernate.annotations.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("reasonParameter")
public class ReasonParameterController {
    @Autowired
    private ReasonParameterService reasonParameterService;

    @PostMapping("/save")
    public String save(@RequestBody ReasonParameters reasonParameters) {
        return reasonParameterService.save(reasonParameters);
    }

    @GetMapping("/byReasonId/{reasonId}")
    public List<ReasonParameterPresenter> getAllByReasonId(@PathVariable("reasonId") Integer reasonID){
        return reasonParameterService.getAllByReasonId(reasonID);
    }
    @GetMapping("/byReasonIdAndWorkerDni/{reasonId}/{workerDni}")
    public ReasonParameterPresenter getByReasonIdAndWorkerDni(@PathVariable Map<String, String> pathVarsMap){
        String reasonId=pathVarsMap.get("reasonId");
        String workerDni=pathVarsMap.get("workerDni");
        Integer reasonID=Integer.parseInt(reasonId);
        return reasonParameterService.getByReasonIdAndWorkerDni(reasonID,workerDni);
    }
}
