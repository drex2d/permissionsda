package ec.edu.espoch.PermissionsDA.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class ReasonParameter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    private Float minimumTime;
    @NotNull
    private Float maximumTime;
    @NotNull
    private Float anticipationTime;
    @NotNull
    private String article;
    @NotNull
    private Boolean chargeToVacation;

    @ManyToOne
    @JoinColumn
    private Reason reason;

    @ManyToOne
    @JoinColumn
    private LaborRelationship laborRelationship;

    @ManyToOne
    @JoinColumn
    private LaborRegime laborRegime;
}
