package ec.edu.espoch.PermissionsDA.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class PermissionTime {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    private Date startDate;
    @NotNull
    private Date startTime;
    @NotNull
    private Date returnDate;
    @NotNull
    private Date returnTime;
    @NotNull
    private Integer permissionTime;

    @ManyToOne
    @JoinColumn
    private Permission permission;
}
