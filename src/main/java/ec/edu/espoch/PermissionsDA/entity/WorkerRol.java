package ec.edu.espoch.PermissionsDA.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class WorkerRol {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idWorkerRol;
    @ManyToOne
    @JoinColumn
    private Worker worker;
    @ManyToOne
    @JoinColumn
    private Rol rol;
}
