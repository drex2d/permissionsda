package ec.edu.espoch.PermissionsDA.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class LaborRegime {
    @Id
    private Integer id;
    private String description;
    @OneToMany(mappedBy = "laborRegime", fetch = FetchType.LAZY)
    private List<WorkerLaborRegime> workerLaborRegimeList;
}