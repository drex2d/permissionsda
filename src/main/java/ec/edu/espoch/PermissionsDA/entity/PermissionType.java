package ec.edu.espoch.PermissionsDA.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class PermissionType {
    @Id
    private Integer id;
    @NotNull
    private String description;

    @OneToMany(mappedBy = "permissionType", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Reason> reason;
}
