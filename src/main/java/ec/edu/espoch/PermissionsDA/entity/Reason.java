package ec.edu.espoch.PermissionsDA.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class Reason {
    @Id
    private Integer id;
    @NotNull
    private String description;
    @NotNull
    private String acronym;

    @ManyToOne
    @JoinColumn
    private PermissionType permissionType;

    @OneToMany(mappedBy = "reason", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Permission> reasonPermissions;

}
