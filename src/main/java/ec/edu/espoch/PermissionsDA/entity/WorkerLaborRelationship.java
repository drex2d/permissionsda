package ec.edu.espoch.PermissionsDA.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class WorkerLaborRelationship {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idWorkerLaborRelationship;
    @OneToOne
    @JoinColumn
    private Worker worker;
    @ManyToOne
    @JoinColumn
    private LaborRelationship laborRelationship;
    @NotNull
    private Date contractStartDate;
    @NotNull
    private Date contractEndDate;
    @NotNull
    private Date startTime;
    @NotNull
    private Date endTime;
}
