package ec.edu.espoch.PermissionsDA.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class WorkerVacation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    @NotNull
    private Integer accumulationYear;
    @NotNull
    private Double accumulatedDays;
    @NotNull
    private Date lastAccumulationDate;
    @NotNull
    private String state;
    @ManyToOne
    @JoinColumn
    private Worker worker;
}
