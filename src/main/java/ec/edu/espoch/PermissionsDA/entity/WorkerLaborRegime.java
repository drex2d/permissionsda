package ec.edu.espoch.PermissionsDA.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class WorkerLaborRegime {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer idWorkerLaborRegime;
    @OneToOne
    @JoinColumn
    private Worker worker;
    @ManyToOne
    @JoinColumn
    private LaborRegime laborRegime;
    @NotNull
    private String jobTitle;
    @NotNull
    private String workDependency;
}